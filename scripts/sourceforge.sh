#!/bin/bash

function sf_get_key() {
    if [ ! -f ~/.ssh/sf_web ]; then
	echo ${SF_KEY} | base64 -d > ~/.ssh/sf_web
    fi
}

function sf_mount() {
    if [ ! -d "repo" ]; then
	mkdir repo
    fi
    if [ ! sf_is_mounted ]; then
	sf_get_key
	sshfs iboyperson@web.sourceforge.net:/home/project-web/archlinux-iboyperson/htdocs/repo \
	      repo \
	      -o StrictHostKeyChecking=no \
	      -o IdentityFile=~/.ssh/sf_web
    fi
}

function sf_umount() {
    if [ sf_is_mounted ]; then
	fusermount3 -u repo
    fi
}

function sf_is_mounted() {
    mountpoint -q repo
    return ${?}
}

function sf_deploy() {
    sf_mount
    
    cp -f $REPODIR/*.db repo
    cp -f $REPODIR/*.files repo
    cp -f $REPODIR/*.pkg.tar.xz repo
}
